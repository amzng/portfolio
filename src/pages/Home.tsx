import React, { useEffect, useRef, useState } from 'react';
import { createContext } from 'vm';
import AboutSection from '../components/home-sections/AboutSection';
import ContactSection from '../components/home-sections/ContactSection';
import IntroSection from '../components/home-sections/IntroSection';
import ProjectSection from '../components/home-sections/ProjectSection';
import StartSection from '../components/home-sections/StartSection';

export type HomeProps = {};

const scrollOpts: ScrollIntoViewOptions = {
  behavior: 'smooth',
  block: 'start',
};

const Home: React.FC<HomeProps> = () => {
  const s1 = useRef<any>(null);
  const s2 = useRef<any>(null);
  const s3 = useRef<any>(null);
  const s4 = useRef<any>(null);

  useEffect(() => {
    const hc = (window as any).addEventListener("hashchange", (e: HashChangeEvent) => {
      console.log(e.newURL, window.location.hash);
      switch(window.location.hash) {
        case "#home": {
          s1?.current?.scrollIntoView(scrollOpts);
          break;
        }
        case "#about": {
          s2?.current?.scrollIntoView(scrollOpts);
          break;
        }
        case "#projects": {
          s3?.current?.scrollIntoView(scrollOpts);
          break;
        }
        case "#contact": {
          s4?.current?.scrollIntoView(scrollOpts);
          break;
        }
        default: {
          s1?.current?.scrollIntoView(scrollOpts);
        }
      }
    });

    return () => {
      window.removeEventListener("hashchange", hc);
    }
  }, []);

  return (
    <>
      <section ref={s1} className="w-full flex justify-center items-center bg-blue-400 snap">
        <IntroSection />
      </section>
      <section ref={s2} className="w-full h-screen flex justify-center items-center bg-gray-800 snap">
        <AboutSection />
      </section>
      <section ref={s3} className="w-full h-screen flex justify-center items-center bg-white-800 snap">
        <ProjectSection />
      </section>
      <section ref={s4} className="w-full h-screen flex justify-center items-center bg-red-900 snap">
        <ContactSection />
      </section>
    </>
  );
}

export default Home;