import React from 'react';

export type CanvasLanderProps = {
  width: number,
  height: number,
  count: number
};

const CanvasLander: React.FC<CanvasLanderProps> = ({
  width,
  height,
  count
}) => {
  return (
    <canvas id="CanvasLander" width={width} height={height}></canvas>
  );
}

export default CanvasLander