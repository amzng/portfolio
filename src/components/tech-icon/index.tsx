import React from 'react';
import { 
  SiSolidity, 
  SiReact, 
  SiNextdotjs, 
  SiEthereum, 
  SiAngular, 
  SiSass,
  SiTailwindcss,
  SiGitlab,
  SiGithub,
  SiTypescript,
  SiCss3,
  SiNestjs,
  SiMongodb,
  SiMysql,
  SiGraphql,
  SiApollographql,
  SiExpress,
  SiSocketdotio,
  SiWebrtc,
  SiJenkins,
  SiVisualstudio,
  SiLinux,
  SiWindows,
  SiAdobephotoshop,
  SiAdobeillustrator,
  SiFigma,
} from 'react-icons/si';
import { IoLogoJavascript, IoLogoNodejs } from 'react-icons/io';
import { AiFillHtml5 } from 'react-icons/ai';
import { FaAws, FaDocker, FaHardHat, FaRust } from 'react-icons/fa';
import { TbBrandReactNative } from 'react-icons/tb';
import { DiJira } from 'react-icons/di';
export enum TechIcons {
  solidity = "solidity",
  react = "react",
  nodejs = "nodejs",
  nextjs = "nextjs",
  ethereum = "ethereum",
  angular = "angular",
  sass = "sass",
  tailwindcss = "tailwindcss",
  github = "github",
  gitlab = "gitlab",
  typescript = "typescript",
  javascript = "javascript",
  html = "html",
  css = "css",
  rust = "rust",
  nestjs = "nestjs",
  mongo = "mongo",
  mysql = "mysql",
  graphql = "graphql",
  apollo = "apollo",
  reactnative = 'reactnative',
  hardhat = "hardhat",
  express = "express",
  socketio = "socketio",
  webrtc = "webrtc",
  jira = "jira",
  jenkins = 'jenkins',
  vsc = "vsc",
  aws = "aws",
  docker = "docker",
  linux = "linux",
  windows = "windows",
  photoshop = "photoshop",
  illustrator = "illustrator",
  figma = "figma",
}

export type TechIconProps = {
  label: string,
  name: TechIcons,
  className?: string
}

const TechIcon: React.FC<TechIconProps> = ({
  label,
  name,
  className = "text-3xl text-gray-700"
}) => {
  return (
    <div className='flex flex-col justify-center items-center mb-4' style={{
      minWidth: '70px'
    }}>
      {name === TechIcons.solidity && (
        <SiSolidity title={label} className={className} />
      )}
      {name === TechIcons.react && (
        <SiReact title={label} className={className} />
      )}
      {name === TechIcons.nodejs && (
        <IoLogoNodejs title={label} className={className} />
      )}
      {name === TechIcons.nextjs && (
        <SiNextdotjs title={label} className={className} />
      )}
      {name === TechIcons.ethereum && (
        <SiEthereum title={label} className={className} />
      )}
      {name === TechIcons.angular && (
        <SiAngular title={label} className={className} />
      )}
      {name === TechIcons.sass && (
        <SiSass title={label} className={className} />
      )}
      {name === TechIcons.tailwindcss && (
        <SiTailwindcss title={label} className={className} />
      )}
      {name === TechIcons.github && (
        <SiGithub title={label} className={className} />
      )}
      {name === TechIcons.gitlab && (
        <SiGitlab title={label} className={className} />
      )}
      {name === TechIcons.javascript && (
        <IoLogoJavascript title={label} className={className} />
      )}
      {name === TechIcons.typescript && (
        <SiTypescript title={label} className={className} />
      )}
      {name === TechIcons.html && (
        <AiFillHtml5 title={label} className={className} />
      )}
      {name === TechIcons.css && (
        <SiCss3 title={label} className={className} />
      )}
      {name === TechIcons.rust && (
        <FaRust title={label} className={className} />
      )}
      {name === TechIcons.nestjs && (
        <SiNestjs title={label} className={className} />
      )}
      {name === TechIcons.mongo && (
        <SiMongodb title={label} className={className} />
      )}  
      {name === TechIcons.mysql && (
        <SiMysql title={label} className={className} />
      )}
      {name === TechIcons.graphql && (
        <SiGraphql title={label} className={className} />
      )}
      {name === TechIcons.apollo && (
        <SiApollographql title={label} className={className} />
      )}
      {name === TechIcons.reactnative && (
        <TbBrandReactNative title={label} className={className} />
      )}
      {name === TechIcons.hardhat && (
        <FaHardHat title={label} className={className} />
      )}
      {name === TechIcons.express && (
        <SiExpress title={label} className={className} />
      )}
      {name === TechIcons.socketio && (
        <SiSocketdotio title={label} className={className} />
      )}
      {name === TechIcons.webrtc && (
        <SiWebrtc title={label} className={className} />
      )}
      {name === TechIcons.jira && (
        <DiJira title={label} className={className} />
      )}
      {name === TechIcons.jenkins && (
        <SiJenkins title={label} className={className} />
      )}
      {name === TechIcons.vsc && (
        <SiVisualstudio title={label} className={className} />
      )}
      {name === TechIcons.aws && (
        <FaAws title={label} className={className} />
      )}
      {name === TechIcons.docker && (
        <FaDocker title={label} className={className} />
      )}
      {name === TechIcons.linux && (
        <SiLinux title={label} className={className} />
      )}
      {name === TechIcons.windows && (
        <SiWindows title={label} className={className} />
      )}
      {name === TechIcons.photoshop && (
        <SiAdobephotoshop title={label} className={className} />
      )}
      {name === TechIcons.illustrator && (
        <SiAdobeillustrator title={label} className={className} />
      )}
      {name === TechIcons.figma && (
        <SiFigma title={label} className={className} />
      )}
      {label && (
        <h4 className='uppercase text-gray-400 font-semibold text-xs mt-2'>{label}</h4>
      )}
    </div>
  );
}

export default TechIcon;