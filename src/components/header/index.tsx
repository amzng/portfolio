import React, { useState } from 'react';
import { CgMenu, CgClose } from 'react-icons/cg';
import { useLocation, useParams, useRoutes } from 'react-router-dom';

export type HeaderProps = {};

const Header: React.FC<HeaderProps> = () => {
  const [drawerOpen, setDrawerOpen] = useState<boolean>(false);
  const loc = useLocation();

  const isActive = (href: string) => {
    console.log(loc.hash, href)
    return loc.hash === href;
  }

  return (
    <div className='h-20 w-full flex justify-between items-center flex-row-reverse md:flex-row px-4 absolute top-0 bg-red-900 header-wrapper'>
      <div className="p-2 px-3 text-white text-2xl border-2 border-white">KG</div>
      <div className={`header-drawer bg-red-900 ${drawerOpen ? 'open' : ''}`} onClick={() => {setDrawerOpen(false)}}>
        <a className={`pr-4 text-white text-3xl header-nav ${isActive("#home") ? 'active' : ''}`} href="#home">HOME</a>
        <a className={`pr-4 text-white text-3xl header-nav ${isActive("#about") ? 'active' : ''}`} href="#about">ABOUT</a>
        <a className={`pr-4 text-white text-3xl header-nav ${isActive("#projects") ? 'active' : ''}`} href="#projects">PROJECTS</a>
        <a className={`pr-4 text-white text-3xl header-nav ${isActive("#contact") ? 'active' : ''}`} href="#contact">CONTACT</a>
      </div>
      <div className='flex md:hidden'>
        {drawerOpen && (
          <CgClose className="text-5xl text-white cursor-pointer" onClick={() => {setDrawerOpen(false)}} />
        )}
        {!drawerOpen && (
          <CgMenu className="text-5xl text-white cursor-pointer" onClick={() => {setDrawerOpen(true)}} />
        )}
      </div>
    </div>
  );
}

export default Header;