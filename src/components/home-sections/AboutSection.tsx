import React from 'react';

export type AboutSectionProps = {};

const AboutSection: React.FC<AboutSectionProps> = () => {
  return (
    <div className='w-full h-screen flex justify-center items-center'>
      <h3 className='text-3xl'>Aboot</h3>
    </div>
  );
}

export default AboutSection