import React from 'react';
import TechIcon, { TechIcons } from '../tech-icon';

export type ProjectSectionProps = {};

const ProjectSection: React.FC<ProjectSectionProps> = () => {
  return (
    <div className='w-full h-screen flex p-6 bg-gray-100'>
      <div className="container">
        <h3 className='text-2xl md:text-3xl font-console text-gray-800 mb-10'>█ Personal Projects</h3>
        <div className="w-full flex flex-row flex-wrap">
          <div className="w-full md:w-1/2 lg:w-1/3 bg-white rounded-md shadow-sm p-4">
            <h2 className='font-console text-gray-800 text-lg mb-4'>PROJECT TITLE</h2>
            <img className="w-full object-cover mb-4" src="https://picsum.photos/300/200" alt="" />
            <h3 className="w-full text-center text-xs font-bold text-gray-400 mb-2">TECHNOLOGIES USED</h3>
            <div className="w-full flex flex-row flex-wrap justify-around">
              <TechIcon name={TechIcons.react} label='React' />
              <TechIcon name={TechIcons.nodejs} label='Node.JS' />
              <TechIcon name={TechIcons.tailwindcss} label='Tailwind CSS' />
              <TechIcon name={TechIcons.sass} label='SASS' />
            </div>
            <h3 className="w-full text-center text-xs font-bold text-gray-400 mb-2">LINKS</h3>
            <div className="w-full flex flex-row flex-wrap justify-around">
              <TechIcon name={TechIcons.gitlab} label='GITLAB' />
              
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProjectSection