import React from 'react';

export type ContactSectionProps = {};

const ContactSection: React.FC<ContactSectionProps> = () => {
  return (
    <div className='w-full h-screen flex justify-center items-center px-4 pt-20 pb-4'>
      <div className="w-full h-full rounded-2xl border-white">
        <h1>CONTACT</h1>
      </div>
    </div>
  );
}

export default ContactSection