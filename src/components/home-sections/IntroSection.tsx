import React from 'react';
import { AiOutlineLinkedin, AiOutlineMail } from 'react-icons/ai';
import TechIcon, { TechIcons } from '../tech-icon';
export type IntroSectionProps = {};

const IntroSection: React.FC<IntroSectionProps> = () => {
  return (
    <div className='w-full max-w-2xl flex justify-center items-center my-20'>
      <div className="container">
        <div className="w-full flex flex-col items-center justify-start bg-white text-black m-2 p-4 rounded-t-xl">
          <div className="w-full flex flex-col justify-start items-center">
            <div className="w-32 h-32 rounded-full">
              <img className="rounded-full" src="/img/me.jpg" alt="" /> 
            </div>
            <h1 className="text-xl font-bold">Kelly Garrett</h1>
            <div className="w-md flex flex-col items-start justify-center">
              <div className="w-md p-2 flex flex-row">
                <AiOutlineLinkedin className='mr-2' size={24} />
                <a href="https://www.linkedin.com/in/kellyrgarrett/">https://www.linkedin.com/in/kellyrgarrett/</a>
              </div>
              <div className="w-md p-2 flex flex-row">
                <AiOutlineMail className='mr-2' size={24} />
                <a href="mailto:krgimports@gmail.com">krgimports@gmail.com</a>
              </div>
            </div>
            <hr className='my-6 w-full'/>
            <div className="max-w-lg w-full">
              <p>Hello, my name is Kelly Garrett and I am a full stack developer located in Austin, Texas. I have worked on projects ranging from small business applications, to large scale applications that serve millions of users. My primary skill set focuses on Javascript / Typescript development on the frontend as well as the backend. </p>
            </div>
            <hr className='my-6 w-full'/>
            <div className="max-w-lg w-full">
              <h1 className="text-lg font-bold my-4 w-full">Languages</h1>
              <div className="flex flex-row flex-wrap">
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.javascript} label='Javascript' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.typescript} label='Typescript' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.solidity} label='Solidity' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.html} label='HTML5' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.css} label='CSS3' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.sass} label='SASS' />
                </div>
              </div>
            </div>
            <div className="max-w-lg w-full">
              <h1 className="text-lg font-bold my-4 w-full">Frameworks</h1>
              <div className="flex flex-row flex-wrap">
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.react} label='React' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.reactnative} label='React Native' />
                </div>                
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.nodejs} label='Node.JS' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.express} label='Express JS' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.socketio} label='Socket.io' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.angular} label='Angular' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.nextjs} label='Next JS' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.nestjs} label='Nest JS' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.graphql} label='Graph QL' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.apollo} label='Apollo' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.hardhat} label='Hardhat' />
                </div>                
              </div>
            </div>
            <div className="max-w-lg w-full">
              <h1 className="text-lg font-bold my-4 w-full">Databases</h1>
              <div className="flex flex-row flex-wrap">
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.mongo} label='Mongo DB' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.mysql} label='MySQL' />
                </div>                
              </div>
            </div>
            <div className="max-w-lg w-full">
              <h1 className="text-lg font-bold my-4 w-full">Technologies</h1>
              <div className="flex flex-row flex-wrap">
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.ethereum} label='Ethereum' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.webrtc} label='WebRTC' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.aws} label='AWS' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.docker} label='Docker' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.linux} label='Linux' />
                </div> 
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.windows} label='Windows' />
                </div> 
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.photoshop} label='Photoshop' />
                </div> 
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.illustrator} label='Illustrator' />
                </div>  
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.figma} label='Figma' />
                </div>          
              </div>
            </div>
            <div className="max-w-lg w-full">
              <h1 className="text-lg font-bold my-4 w-full">CICD</h1>
              <div className="flex flex-row flex-wrap">
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.gitlab} label='GitLab' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.github} label='GitHub' />
                </div>
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.jira} label='Jira' />
                </div> 
                <div className="w-20 h-20 flex items-center justify-center">
                  <TechIcon name={TechIcons.jenkins} label='Jenkins' />
                </div>          
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default IntroSection