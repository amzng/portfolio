import { FreeCamera, HemisphericLight, Vector3, StandardMaterial, Texture, Color3, MeshBuilder, CannonJSPlugin } from '@babylonjs/core';
import React from 'react';
import 'babylonjs-loaders';
import SceneComponent from '../scene-component';
import * as cannon from "cannon";

export type StartSectionProps = {};

const StartSection: React.FC<StartSectionProps> = () => {
  const onSceneReady = (scene: any) => {
    // This creates and positions a free camera (non-mesh)
    const camera = new FreeCamera("camera1", new Vector3(0, 15, -50), scene);
    scene.enablePhysics();
  
    // This targets the camera to scene origin
    camera.setTarget( new Vector3(0, 15, -40));
  
    const canvas = scene.getEngine().getRenderingCanvas();
  
    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    var light = new HemisphericLight("light", new Vector3(0, 1, 0), scene);
  
    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;
    const nodejs = new StandardMaterial("nodejs", scene);
    nodejs.diffuseColor = new Color3(0,0,0);
    nodejs.emissiveTexture = new Texture("/nodejs.png", scene, undefined, true);

    const box = MeshBuilder.CreateBox('nodejs-img', {size: 5}, scene);
    box.material = nodejs;
    box.position.y = 3
   /* 	//leaf material
    var green = new StandardMaterial("green", scene);
    green.emissiveTexture = new Texture("https://secure.img1-fg.wfcdn.com/im/68725640/resize-h755-w755%5Ecompr-r85/8421/84212273/Kersten+Skulls+Teens+Removable+Peel+and+Stick+Wallpaper+Panel.jpg", scene);
    green.diffuseColor = new Color3(0,1,1);	
    
    //trunk and branch material
    var bark = new StandardMaterial("bark", scene);
    bark.emissiveTexture = new Texture("https://secure.img1-fg.wfcdn.com/im/68725640/resize-h755-w755%5Ecompr-r85/8421/84212273/Kersten+Skulls+Teens+Removable+Peel+and+Stick+Wallpaper+Panel.jpg", scene);
    bark.diffuseTexture = new Texture("https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Bark_texture_wood.jpg/800px-Bark_texture_wood.jpg", scene);
    // bark.diffuseTexture.uScale = 2.0;//Repeat 5 times on the Vertical Axes
    // bark.diffuseTexture.vScale = 2.0;//Repeat 5 times on the Horizontal Axes		
                  
    var trunk_height = 15;
    var trunk_taper = 0.666;
    var trunk_slices = 5;
    var boughs = 2; // 1 or 2
	var forks = 4;
	var fork_angle = Math.PI/4;
    var fork_ratio = 3/(1+Math.sqrt(5)); //PHI the golden ratio
	var branch_angle = Math.PI/3;
	var bow_freq = 3;
	var bow_height = 3.5;
	var branches = 20;
	var leaves_on_branch = 50;
    var leaf_wh_ratio = 0.5;
                  
    tree = createTree(trunk_height, trunk_taper, trunk_slices, bark, boughs, forks, fork_angle, fork_ratio, branches, branch_angle, bow_freq, bow_height, leaves_on_branch, leaf_wh_ratio, green, scene);				               
      tree.position.y = 0;*/
    
    // Our built-in 'ground' shape.
    MeshBuilder.CreateGround("ground", { width: 6, height: 6 }, scene);
    return scene;
  };

  const onRender = (scene:any ) => {

  };


  return (
    <div className='w-full h-full'>
      <SceneComponent className="w-full h-full" antialias onSceneReady={onSceneReady} onRender={onRender} id="my-canvas" />
      <div className="w-screen h-screen flex items-center justify-center absolute top-0 left-0 z-0">
        This is the title
      </div>
    </div>
  );
}

export default StartSection;