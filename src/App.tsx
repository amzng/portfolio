import React from "react";
import Home from './pages/Home';
import Header from './components/header';

import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';

function App() {
  return (
    <>
    <Header />
    <div className="scrollbox">
      <Routes>
        <Route path="/" element={(<Home />)} />
      </Routes>
    </div>
    </>
  );
}

export default App;
